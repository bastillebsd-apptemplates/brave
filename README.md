## Brave

Bastille App Template to bootstrap Brave

## Need Pulseaudio

```shell
pkg install pulseaudio
```
## Create

```shell
bastille create -L TARGET focal 10.17.89.26 
```
## Usage

```shell
bastille template TARGET https://gitlab.com/bastillebsd-apptemplates/brave.git --arg XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR --arg PA_SOCK_PATH=$XDG_RUNTIME_DIR/pulse/ --arg PA_COOKIE=~/.config/pulse
```

## Allow Xorg Jail to Host (exec like you user)

```shell
 xhost +
```
## Launch Brave (Download or Copy the launch_brave Script)

```shell
 launcher/launch_brave TARGET
```

